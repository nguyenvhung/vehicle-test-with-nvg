//
//  VehicleTestWithNVGTests.swift
//  VehicleTestWithNVGTests
//
//  Created by Hung Nguyen on 24/12/2021.
//

import XCTest
@testable import VehicleTestWithNVG

class VehicleTestWithNVGTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testGetVehicles() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results:
        var service = VehicleService()
        var mockVehicleRequest = VehicleService.VehicleRequestQuery(make: 9, model: 1562, skip: 0, take: 20)
        let expectation = expectation(description: "vehicle items")
        var itemsResponse: [Vehicle]?
        
        service.getVehicleList(requestQuery: mockVehicleRequest, completion: { items  in
            itemsResponse = items
            expectation.fulfill()
        }, failure: { error in
            XCTAssertNil(error)
        })
        waitForExpectations(timeout: 5) { (error) in
            XCTAssertNotNil(itemsResponse)
          }
    }
    
    func testMakeURL() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results:
        var mockVehicleRequest = VehicleService.VehicleRequestQuery(make: 9, model: 1562, skip: 0, take: 20)
        var requestQuery = mockVehicleRequest
        guard let url = RequestURL(type: .getVehicleList).getURL(params: requestQuery.asParamList()) else {
            return
        }
        let targetURL = String(format: "http://testautoscout.azurewebsites.net/api/vehicles?make=%@&model=%@&skip=%@&take=%@", "9", "1562", "0", "20")
        print(url.absoluteString)
        print(targetURL)
        XCTAssert(url.absoluteString == targetURL)
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here:
            var service = VehicleService()
            var mockVehicleRequest = VehicleService.VehicleRequestQuery(make: 9, model: 1562, skip: 0, take: 20)
            var itemsResponse: [Vehicle]?
            
            service.getVehicleList(requestQuery: mockVehicleRequest, completion: { items  in
                itemsResponse = items
            }, failure: { error in
            })
        }
    }

}
