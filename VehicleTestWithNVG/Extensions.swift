//
//  Extensions.swift
//  VehicleTestWithNVG
//
//  Created by Hung Nguyen on 24/12/2021.
//
 
import Foundation

extension String {
    func trimAllSpaces() -> String {
        return self.filter { !$0.isWhitespace }
    }
}

extension Double {
    var string: String {
        return String(describing: self)
    }
}

extension Int {
    var string: String {
        return String(describing: self)
    }
}
