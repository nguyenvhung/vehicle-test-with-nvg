//
//  Vehicle.swift
//  VehicleTestWithNVG
//
//  Created by Hung Nguyen on 24/12/2021.
//

import Foundation
import ObjectMapper

class Vehicle: Mappable {
    var id: Int?
    var make: String?
    var model: String?
    var firstRegistrationYr: Int?
    var firstRegistrationMnth: Int?
    var km: Int?
    var price: Int?
    
    var firstRegistrationDateString: String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd"
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "MMM yyyy"
        if let year = self.firstRegistrationYr, let month = self.firstRegistrationMnth {
            let date = formatter.date(from: "\(year)/\(month)/01")
            return (dateFormatterPrint.string(from: date!))
        } else {
            let date = Date()
            return (dateFormatterPrint.string(from: date))
        }
    }
    
    init() {}
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        id <- map["id"]
        make <- map["make"]
        model <- map["model"]
        km <- map["km"]
        firstRegistrationYr <- map["firstRegYear"]
        firstRegistrationMnth <- map["firstRegMonth"]
        price <- map["price"]
        
    }
}
