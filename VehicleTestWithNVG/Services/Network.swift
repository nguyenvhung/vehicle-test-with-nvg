//
//  VehicleServices.swift
//  VehicleTestWithNVG
//
//  Created by Hung Nguyen on 24/12/2021.
//

import Foundation
import Alamofire
import SwiftyJSON

class Network {
    
    typealias NetworkCompletion<T> = (T)->Void
    typealias NetworkFailure = (RequestError)->Void
    
     func get(url: URL, completion: @escaping NetworkCompletion<JSON>, failure: @escaping NetworkFailure) {
        var req = URLRequest(url: url)
        req.httpMethod = "GET"
        AF.request(req).responseJSON(completionHandler: { response in 
            if let error = response.error as NSError? {
                failure(RequestError(code: "", message: "Cannot make get request", error: error))
            }
            if let code = response.response?.statusCode, let data =  response.data, let json = try? JSON(data: data) {
                if code != 200 {
                    let message = json["Message"].stringValue
                    failure(RequestError(code: "400", message: message, error: nil))
                } else {
                    completion(json)
                }
            }
        })
    }

     func post(url: URL, completion: @escaping NetworkCompletion<JSON>, failure: @escaping NetworkFailure) {
        var req = URLRequest(url: url)
        req.httpMethod = "POST"
        AF.request(req).responseJSON(completionHandler: { response in
            if let error = response.error as NSError? {
                failure(RequestError(code: "", message: "", error: error))
            }
            if let code = response.response?.statusCode, let data =  response.data, let json = try? JSON(data: data) {
                if code != 200 {
                    let message = json["Message"].stringValue
                    failure(RequestError(code: "400", message: message, error: nil))
                } else {
                    completion(json)
                }
            }
        })
    }
}
