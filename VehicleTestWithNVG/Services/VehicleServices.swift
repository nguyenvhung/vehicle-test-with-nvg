//
//  VehicleServices.swift
//  VehicleTestWithNVG
//
//  Created by Hung Nguyen on 24/12/2021.
//

import Foundation
import UIKit
import CoreLocation
import Alamofire
import SwiftyJSON

class RequestError: Error {
    var message: String = "Unknown error"
    var code: String = ""
    var error: NSError?
    
    init(code: String, message: String, error: NSError?) {
        self.code = code
        self.message = message
        if let _ = error {
            self.error = error
        }
    }
}

class RequestURL {
    var requestType: RequestURLEnum?
    
    init(type: RequestURLEnum) {
        self.requestType = type
    }
    
    enum RequestURLEnum: String {
        case getVehicleList = "http://testautoscout.azurewebsites.net/api/vehicles?make=%@&model=%@&skip=%@&take=%@"
    }
    
    func getURL(params: [String] = [])  -> URL? {
        if params.count >= 1 {
            switch self.requestType {
            case .getVehicleList:
                let str = String(format: self.requestType?.rawValue ?? "", params[0],params[1], params[2], params[3])
                return URL(string: str)
            default:
                return nil
            }
        } else {
            return nil
        }
    }
}

class VehicleService {
    var api = Network()
    typealias ApiCompletion<T> = (T)->Void
    
    class VehicleRequestQuery {
        var make: Int
        var model: Int
        var skip: Int
        var take: Int
        
        init(make: Int, model: Int, skip: Int, take: Int) {
            self.make = make
            self.model = model
            self.skip = skip
            self.take = take
        }
        
        func asParamList() -> [String] {
            return ["\(make)", "\(model)", "\(skip)", "\(take)"]
        }
    }
    
    public func getVehicleList(requestQuery: VehicleRequestQuery, completion: @escaping ApiCompletion<[Vehicle]>, failure: @escaping (RequestError)->Void) {
        guard let url = RequestURL(type: .getVehicleList).getURL(params: requestQuery.asParamList()) else {
            print("Failed to get url with these infos: \(requestQuery.asParamList())")
            failure(RequestError(code: "", message: "Unable to get vehicle list", error: nil))
            return
        }
        api.get(url: url, completion: { json in
            let array: [Vehicle] = json["results"].arrayValue.map { eachJSON in
                let info: Vehicle = Vehicle(JSONString: eachJSON.description) ?? Vehicle()
                return info
            }
            completion(array)
        }, failure: { error in
            failure(error)
        })
    }
    
}

