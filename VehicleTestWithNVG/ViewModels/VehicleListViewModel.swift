//
//  VehicleServices.swift
//  VehicleTestWithNVG
//
//  Created by Hung Nguyen on 24/12/2021.
//

import Foundation
import Combine
import Alamofire
import SwiftyJSON
import AlamofireURLCache5
import CoreLocation

class VehicleListViewModel : ObservableObject {
    private var currentLocation: CLLocation?
    
    private var service = VehicleService()
    
    @Published var list = [Vehicle]()
    
    func getVehicleList(completion: @escaping (([Vehicle])->Void), failure: ((RequestError)->Void)?) {
        let mockVehicleRequest = VehicleService.VehicleRequestQuery(make: 9, model: 1562, skip: 0, take: 20)
        service.getVehicleList(requestQuery: mockVehicleRequest, completion: { items  in
            if failure != nil {
                failure!(RequestError(code: "", message: "", error: nil))
            }
            completion(items)
        }, failure: { error in
            if failure != nil {
                failure!(error)
            }
        })
    }
}
