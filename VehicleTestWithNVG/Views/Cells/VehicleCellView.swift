//
//  VehicleCellView.swift
//  VehicleTestWithNVG
//
//  Created by Hung Nguyen on 24/12/2021.
//

import SwiftUI
import Foundation

struct VehicleView: View {
    private let vehicle: Vehicle
    @State private var blurRadius: Int = 1
    
    init(vehicle: Vehicle) {
        self.vehicle = vehicle
    }
    
    var body: some View {
        HStack {
            VStack(alignment: .leading, spacing: 15) {
                Spacer().frame(width: 1, height: 5)
                Text(vehicle.make ?? "")
                    .font(.system(size: 18))
                HStack(spacing: 25) {
                    VStack(alignment: .leading) {
                        Text("1st Registration").font(.system(size: 14)).foregroundColor(Color.gray)
                        Text(vehicle.firstRegistrationDateString)
                            .font(.system(size: 14))
                    }
                    VStack(alignment: .leading) {
                        Text("KM").font(.system(size: 14)).foregroundColor(Color.gray)
                        Text("\(vehicle.km ?? 0) km")
                            .font(.system(size: 14))
                    }
                    VStack(alignment: .leading) {
                        Text("Model").font(.system(size: 14)).foregroundColor(Color.gray)
                        Text("\(vehicle.model ?? "")")
                            .fontWeight(.bold).font(.system(size: 14))
                    }
                }
                Spacer().frame(width: 1, height: 5)
            }
        }
    }
}
