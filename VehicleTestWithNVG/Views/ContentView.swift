//
//  ContentView.swift
//  VehicleTestWithNVG
//
//  Created by Hung Nguyen on 24/12/2021.
//

import SwiftUI

struct ContentView: View {
    @ObservedObject var viewModel = VehicleListViewModel()
    @ObservedObject var keyboardHeightGetter = KeyboardHeightHelper()
    @State var errorMessage: String = ""
    @State private var searchText = ""
    @State private var list: [Vehicle] = []
    
    var searchResults: [Vehicle] {
            if searchText.isEmpty {
                return list
            } else {
                return list.filter { $0.model?.contains(searchText.lowercased()) ?? false
                    || $0.make?.contains(searchText.lowercased()) ?? false
                    || $0.firstRegistrationDateString.contains(searchText.lowercased())
                }
            }
        }
    
    var body: some View {
        ZStack(alignment: .bottom) {
            NavigationView {
                List(searchResults, id: \.id) { each in
                    VehicleView(vehicle: each)
                }
                .searchable(text: $searchText)
                .navigationBarTitle("Vehicles")
                .onAppear {
                    self.viewModel.getVehicleList(completion: { vehicleList in
                        self.list = vehicleList
                    }, failure: { error in
                        self.errorMessage = error.message
                    })
                }
            }
            VStack {
                Text("\(self.errorMessage)")
                    .foregroundColor(.white)
                    .padding()
            }
            .frame(width: UIScreen.main.bounds.width - 32, alignment: .center)
            .background(SwiftUI.Color.red)
            .cornerRadius(12)
            .padding()
            .opacity(self.errorMessage.trimAllSpaces() == "" ? 0 : 1)
        }
    }
}

