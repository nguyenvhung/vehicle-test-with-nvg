# Project: Vehicle List with NVG

Below are some specifications of this project:

## Architecture:
The projects uses MVVM architecture with a view model for the only view, `ContentView`, which is a list view of vehicle data. Besides, some auxiliary models were made for assistant procedures:
- API fetching 
- Object mapping from server’s JSON to designated classes.
- etc.

## Patterns
Observable pattern is used in the project with help of Combine library which was accompanied with SwiftUI framework.

For more details, the View listens to its designated View Model via Published subjects created by Combine. The View Model itself is declared in the View as an Observed Object.

In addition, in the ordinary UIKit, all UI elements will be notified when a variable inside such a ViewController class is updated; in order to achieve this in SwiftUI, it needs to use @State annotation added before the internal variable of the View class.

## Structure
The project’s tree folder is already arranged as Models, Views, ViewModels and Services as the developer can find it easy to change and continuously develop the app. Inside the Views folder, some sub-UI classes are also stored.
The home screen of the app consists of a search bar which can help users to find vehicle based on a specific keyword in either name, make or year of first registration.

The libraries as below are used in the project to help making things done in a comfortable way:
- `SwiftyJSON`: helps to get data in primitive Swift types from JSONs
- `ObjectMapper`: helps to map/query data from JSON string to classes easily.
- `AlamoFire`: replaces URLSession in getting data from server.

## Steps to run the project
1. Make sure Xcode 12 or above is installed.
2. Inside the project folder, run `pod install` on Intel Mac, in case of Silicon Mac, please run this command with x86_64 option.
3. Open the `.xcworkspace` file.
4. Run the project with pressing Play button on the main toolbar.

## Some highlighted outputs:
- [x] Programming language: Swift is required, Objective-C is optional. 
- [x] Design app's architecture with MVVM
- [x] A simple and minimalistic UI
- [x] Write some UnitTests 
- [x] Exception handling 
- [x] Caching handling
- [x] Readme file 

